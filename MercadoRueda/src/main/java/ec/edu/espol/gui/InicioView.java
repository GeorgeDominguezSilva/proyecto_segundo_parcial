/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.gui;

import ec.edu.espol.gui.auth.LoginView;
import ec.edu.espol.gui.auth.RegisterView;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Usuario
 */
public class InicioView extends PaneOrganizer{
    private BorderPane root;
    
    
    public InicioView(){
        root = new BorderPane();
        root.getStylesheets().add("/styles/Inicio.css");
        //root.setAlignment(Pos.BASELINE_CENTER);
        //root.setSpacing(20);
        Label lblTitulo = new Label("MercadoRueda.com");
        Button btnLogin = new Button("Iniciar Sesion");
        Button btnRegistro = new Button("Registrarse");
        
        HBox panelSuperior = new HBox();
        panelSuperior.setAlignment(Pos.CENTER);
        panelSuperior.getChildren().add(lblTitulo);
        
        HBox panelInferior = new HBox();
        panelInferior.setAlignment(Pos.CENTER);
        panelInferior.setPadding(new Insets(20,20,20,20));
        panelInferior.setSpacing(20);
        panelInferior.getChildren().addAll(btnLogin, btnRegistro);
        
        //no es necesario controller no hay complejidad
        btnLogin.setOnAction(e->{nextScene(root,new LoginView());});
        btnRegistro.setOnAction(e->{nextScene(root,new RegisterView());});
        //root.getChildren().addAll(lblTitulo,btnLogin,btnRegistro);
        
        root.setTop(panelSuperior);
        root.setBottom(panelInferior);
    }

    @Override
    public Pane getRoot() {
        return root;
    }
    
}
