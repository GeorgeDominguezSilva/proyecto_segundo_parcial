/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.gui.user.comprador;

/**
 *
 * @author Usuario
 */

import ec.edu.espol.model.Vehiculo;
import ec.edu.espol.model.vehiculos.Auto;
import ec.edu.espol.model.vehiculos.Camion;
import ec.edu.espol.model.vehiculos.Camioneta;
import ec.edu.espol.model.vehiculos.Motocicleta;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;


import java.io.FileInputStream;

public class LayoutVehiculo extends VBox {
    private Vehiculo producto;
    public LayoutVehiculo(Vehiculo producto){
        //
        this.producto = producto;        
        this.getStylesheets().add("styles/LayoutVehiculo.css");
        this.setPrefSize(125,125);
        this.setAlignment(Pos.CENTER);
        //this.setSpacing(5);
        Label lblTitulo = new Label(producto.getPlaca());
        Label lblPrecio = new Label("$"+String.format ("%.2f", producto.getPrecio()));
        //Label lblPrecio = new Label("$"+producto.getPrecio());

        ImageView iv = new ImageView();
        iv.setFitWidth(60);
        iv.setFitHeight(60);
        //iv.setPreserveRatio(true);

        try{
            //Image im;
            //Image im = new Image(new FileInputStream("productos/"+producto.getPlaca()+".png"));;
            String rutaCustom = "productos/"+producto.getPlaca()+".png";
            if (producto instanceof Auto){
                System.out.println("auto");
                rutaCustom = "Auto";
            }else if (producto instanceof Camion){
                System.out.println("camion");
                rutaCustom = "Camion";
            }else if (producto instanceof Camioneta){
                System.out.println("camioneta");
                rutaCustom = "Camioneta";
            }else if (producto instanceof Motocicleta){
                System.out.println("moto");
                rutaCustom = "Motocicleta";
            }
            Image im = new Image("/img/vehiculos/"+rutaCustom+".png");            
            iv.setImage(im);
        } catch (Exception ex){            
            Image imr = new Image("/img/error.png");
            iv.setImage(imr);
        }
        this.getChildren().add(iv);
        this.getChildren().add(lblTitulo);
        this.getChildren().add(lblPrecio);
    }

    public Vehiculo getProducto() {
        return producto;
    }
}

