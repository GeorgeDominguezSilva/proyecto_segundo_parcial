/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.gui.user.vendedor;

import ec.edu.espol.model.Oferta;
import ec.edu.espol.model.Vehiculo;
import ec.edu.espol.model.vehiculos.Auto;
import ec.edu.espol.model.vehiculos.Camion;
import ec.edu.espol.model.vehiculos.Camioneta;
import ec.edu.espol.model.vehiculos.Motocicleta;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 *
 * @author Usuario
 */

import ec.edu.espol.model.Vehiculo;
import ec.edu.espol.model.vehiculos.Auto;
import ec.edu.espol.model.vehiculos.Camion;
import ec.edu.espol.model.vehiculos.Camioneta;
import ec.edu.espol.model.vehiculos.Motocicleta;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;


import java.io.FileInputStream;

public class LayoutOferta extends VBox {
    private Oferta oferta;
    public LayoutOferta(Oferta oferta){
        //
        this.oferta = oferta;        
        this.getStylesheets().add("styles/LayoutOferta.css");
        this.setPrefSize(125,125);
        this.setAlignment(Pos.CENTER);
        //this.setSpacing(5);
        Label lblTitulo = new Label(oferta.getComprador());
        Label lblPrecio = new Label("$"+String.format ("%.2f", oferta.getPrecio()));
        //Label lblPrecio = new Label("$"+oferta.getPrecio());

        ImageView iv = new ImageView();
        iv.setFitWidth(60);
        iv.setFitHeight(60);
        //iv.setPreserveRatio(true);

        try{            
            Image im = new Image("/img/ofertas/Oferta.png");            
            iv.setImage(im);
        } catch (Exception ex){            
            Image imr = new Image("/img/error.png");
            iv.setImage(imr);
        }
        this.getChildren().add(iv);
        this.getChildren().add(lblTitulo);
        this.getChildren().add(lblPrecio);
    }

    public Oferta getOferta() {
        return oferta;
    }
}