/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.gui.user;

import ec.edu.espol.controller.AuthController;
import ec.edu.espol.gui.InicioView;
import ec.edu.espol.gui.PaneOrganizer;
import ec.edu.espol.model.Persona;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 *
 * @author Usuario
 */
public class ChangePasswordView  extends PaneOrganizer{
    
    private Persona usuario;

    private BorderPane root;
    private HBox panelSuperior;
    private GridPane panelCentral;
    
    public ChangePasswordView(Persona persona){
        root = new BorderPane();
        this.usuario = persona;
        root.getStylesheets().add("styles/Auth.css");
        //panel superior
        panelSuperior = new HBox();                
        //panel central
        panelCentral = new GridPane();
        
        //load components
        cargarPanelSuperior();
        cargarPanelCentral();
        
        //root loading
        root.setTop(panelSuperior);
        root.setCenter(panelCentral);
    }
    
    void cargarPanelSuperior(){
        panelSuperior.setAlignment(Pos.BASELINE_CENTER);
        panelSuperior.setSpacing(20);
        panelSuperior.setPadding(new Insets(20,20,20,20));
        
        Label lblTitulo = new Label("Seguridad");
        lblTitulo.getStyleClass().add("titulo");
        panelSuperior.getChildren().add(lblTitulo);
    }
    
    void cargarPanelCentral(){
        panelCentral.setAlignment(Pos.CENTER);
        panelCentral.setHgap(20);
        panelCentral.setVgap(20);
        
        Label lblCurrentPassword = new Label("Clave");
        Label lblNewPassword = new Label("Nueva: ");
        PasswordField txtCurrentPassword = new PasswordField();
        PasswordField txtNewPassword = new PasswordField();
        
        panelCentral.add(lblCurrentPassword, 0, 0); //pane,col,row
        panelCentral.add(txtCurrentPassword, 1, 0); //pane,col,row        
        panelCentral.add(lblNewPassword, 0, 1); //pane,col,row
        panelCentral.add(txtNewPassword, 1, 1); //pane,col,row
        
        Button btnUpdate = new Button("Actualizar");
        btnUpdate.setOnAction(e->{AuthController.changePassword(usuario, txtCurrentPassword.getText(), txtNewPassword.getText(), this );});
        Button btnSalir =    new Button("Regresar");
        btnSalir.getStyleClass().add("btn-salir");        
        btnSalir.setOnAction(e->nextScene(root,new InicioView()));
        panelCentral.add(btnUpdate, 1, 2); //pane,col,row
        panelCentral.add(btnSalir, 1, 3); //pane,col,row
        
    }
    
    @Override
    public Pane getRoot() {
        return root;
    }    
    
}
