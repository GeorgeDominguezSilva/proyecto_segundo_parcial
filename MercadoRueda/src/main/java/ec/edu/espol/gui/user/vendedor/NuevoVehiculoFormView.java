/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.gui.user.vendedor;

import ec.edu.espol.controller.AuthController;
import ec.edu.espol.controller.VendedorController;
import ec.edu.espol.gui.InicioView;
import ec.edu.espol.gui.PaneOrganizer;
import ec.edu.espol.gui.user.DashboardView;
import ec.edu.espol.model.Persona;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 *
 * @author Usuario
 */
public class NuevoVehiculoFormView extends PaneOrganizer{
    
    //
    private Persona usuario;

    private BorderPane root;
    private HBox panelSuperior;
    private GridPane panelCentral;
    
    private String tipo;
    
    public NuevoVehiculoFormView(Persona persona, String tipo){
        root = new BorderPane();
        this.usuario = persona;
        this.tipo = tipo;
        root.getStylesheets().add("styles/Auth.css");
        //panel superior
        panelSuperior = new HBox();                
        //panel central
        panelCentral = new GridPane();
        
        //load components
        cargarPanelSuperior();
        cargarPanelCentral();
        
        //root loading
        root.setTop(panelSuperior);
        root.setCenter(panelCentral);
    }
    
    void cargarPanelSuperior(){
        panelSuperior.setAlignment(Pos.BASELINE_CENTER);
        panelSuperior.setSpacing(20);
        panelSuperior.setPadding(new Insets(20,20,20,20));
        
        Label lblTitulo = new Label("Formulario Crear "+tipo);
        lblTitulo.getStyleClass().add("titulo");
        panelSuperior.getChildren().add(lblTitulo);
    }
    
    void cargarPanelCentral(){
        panelCentral.setAlignment(Pos.CENTER);
        panelCentral.setHgap(20);
        panelCentral.setVgap(10);
        
        //public Vehiculo(String vendedor, String placa, String marca, String modelo,
        //String tipoMotor, int anio, double recorrido, String color,
        //String tipoCombustible, double precio, String transmision, int vidrios) {
        Label lblPlaca = new Label("Placa");
        TextField txtPlaca = new TextField("");
        
        Label lblMarca = new Label("Marca");
        TextField txtMarca = new TextField("");
        
        Label lblModelo = new Label("Modelo");
        TextField txtModelo = new TextField("");        
        
        Label lblTipoMotor = new Label("Tipo Motor");
        TextField txtTipoMotor = new TextField("");
        
        Label lblAnio = new Label("Anio"); //validar
        TextField txtAnio = new TextField("2020");
        
        Label lblRecorrido = new Label("Recorrido"); //validar
        TextField txtRecorrido = new TextField("0.00");
        
        Label lblColor = new Label("Color");
        TextField txtColor = new TextField("");
        
        Label lblTipoCombustible = new Label("Tipo Combustible");
        TextField txtTipoCombustible = new TextField("");
        
        Label lblPrecio = new Label("Precio: $"); //validar
        TextField txtPrecio = new TextField("0.00");
        
        Label lblTransmision = new Label("Transmision");
        TextField txtTransmision = new TextField("");
        
        Label lblVidrios = new Label("Vidrios"); //validar
        TextField txtVidrios = new TextField("0");
        
        if (tipo.equals("Motocicleta")){
            lblVidrios.setVisible(false);
            txtVidrios.setVisible(false);
        }
        
        //Label lblCurrentPassword = new Label("Clave");
        //Label lblNewPassword = new Label("Nueva: ");
        //PasswordField txtCurrentPassword = new PasswordField();
        //PasswordField txtNewPassword = new PasswordField();
        
        panelCentral.add(lblPlaca, 0, 0); //pane,col,row
        panelCentral.add(txtPlaca, 1, 0); //pane,col,row        
        
        panelCentral.add(lblMarca, 0, 1); //pane,col,row
        panelCentral.add(txtMarca, 1, 1); //pane,col,row        
        
        panelCentral.add(lblModelo, 0, 2); //pane,col,row
        panelCentral.add(txtModelo, 1, 2); //pane,col,row 
        
                
        panelCentral.add(lblTipoMotor, 0, 3); //pane,col,row
        panelCentral.add(txtTipoMotor, 1, 3); //pane,col,row 
        
        panelCentral.add(lblAnio, 0, 4); //pane,col,row
        panelCentral.add(txtAnio, 1, 4); //pane,col,row 
        
        panelCentral.add(lblRecorrido, 0, 5); //pane,col,row
        panelCentral.add(txtRecorrido, 1, 5); //pane,col,row 
        
        panelCentral.add(lblColor, 0, 6); //pane,col,row
        panelCentral.add(txtColor, 1, 6); //pane,col,row 
        
        panelCentral.add(lblTipoCombustible, 0, 7); //pane,col,row
        panelCentral.add(txtTipoCombustible, 1, 7); //pane,col,row
        
        panelCentral.add(lblPrecio, 0, 8); //pane,col,row
        panelCentral.add(txtPrecio, 1, 8); //pane,col,row
        
        panelCentral.add(lblTransmision, 0, 9); //pane,col,row
        panelCentral.add(txtTransmision, 1, 9); //pane,col,row
        
        panelCentral.add(lblVidrios, 0, 10); //pane,col,row
        panelCentral.add(txtVidrios, 1, 10); //pane,col,row
        
        
        
        
        
        
        Button btnCrear = new Button("Guardar");
        btnCrear.setOnAction(e->{
            //VendedorController.crearVehiculo();
            //validar formulario como son muy largos prefiero no crear otro metodo y aumentar complejidad
            String placa = txtPlaca.getText();
            String marca = txtMarca.getText();
            String modelo = txtModelo.getText();
            String tipoMotor = txtTipoMotor.getText();            
            String color = txtColor.getText();
            String tipoCombustible = txtTipoCombustible.getText();
            String transmision = txtTransmision.getText();
            
            double precio = 0;
            double recorrido = 0;            
            int anio = 0;            
            //np
            int vidrios = 0;
            
            if(placa.isEmpty()){
                VendedorController.showAlert("Formulario", "Placa", "La placa no debe estar vacia");
                return;
            }
            
            try{
                precio = Double.parseDouble(txtPrecio.getText());
            } catch (NumberFormatException ex){
                VendedorController.showAlert("Formulario", "Precio", "El precio debe ser decimal. Ejm: 1000.00");
                return;
            }
            
            try{
                recorrido = Double.parseDouble(txtRecorrido.getText());
            } catch (NumberFormatException ex){
                VendedorController.showAlert("Formulario", "Recorrido", "El recorrido debe ser decimal. Ejm: 300.00");
                return;
            }
            
            try{
                anio = Integer.parseInt(txtAnio.getText());
            } catch (NumberFormatException ex){
                VendedorController.showAlert("Formulario", "Anio", "El anio debe ser numerico");
                return;
            }
            
            try{
                vidrios = Integer.parseInt(txtVidrios.getText());
            } catch (NumberFormatException ex){
                VendedorController.showAlert("Formulario", "Vidrios", "La cantidad de vidrios debe ser numerico");
                return;
            }
            
            VendedorController.crearVehiculo(tipo, usuario , placa, marca, modelo, tipoMotor, anio, recorrido, color, tipoCombustible, precio, transmision, vidrios, this);
            
            
        });
        
        Button btnSalir =    new Button("Regresar");
        btnSalir.getStyleClass().add("btn-salir");        
        btnSalir.setOnAction(e->nextScene(root, new DashboardView(usuario) ));
        panelCentral.add(btnCrear, 4, 0); //pane,col,row
        panelCentral.add(btnSalir, 4, 1); //pane,col,row
        
    }
    
    @Override
    public Pane getRoot() {
        return root;
    }
    
    
    
}
