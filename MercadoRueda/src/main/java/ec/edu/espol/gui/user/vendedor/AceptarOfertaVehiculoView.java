/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.gui.user.vendedor;

import ec.edu.espol.controller.CompradorController;
import ec.edu.espol.controller.VendedorController;
import ec.edu.espol.gui.PaneOrganizer;
import ec.edu.espol.gui.user.DashboardView;
import ec.edu.espol.gui.user.comprador.EnviarPujaVehiculoView;
import ec.edu.espol.gui.user.comprador.LayoutVehiculo;
import ec.edu.espol.model.Oferta;
import ec.edu.espol.model.Persona;
import ec.edu.espol.model.Vehiculo;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Usuario
 */
public class AceptarOfertaVehiculoView extends PaneOrganizer{
    
    private BorderPane root;
    private Persona usuario;
    
    public AceptarOfertaVehiculoView(Persona persona){
        root = new BorderPane();        
        this.usuario = persona;
        root.getStylesheets().add("styles/PujarVehiculo.css");
        
        cargarPanelSuperior();
        
        cargarPanelInferior();
        
        //opcional        
    }
    
    private void cargarPanelInferior(){
        BorderPane panel = new BorderPane();
        panel.setStyle("-fx-background-color: #000046");
        Button btnSalir = new Button("Salir");
        btnSalir.getStyleClass().add("btn-salir");
        btnSalir.setPrefWidth(220);
                
        panel.setLeft(btnSalir);
        
        
        
        root.setBottom(panel);
        btnSalir.setOnAction(e->salir());
        
    }

    
    private void cargarPanelSuperior(){
        GridPane panel = new GridPane();
        panel.setAlignment(Pos.CENTER);
        panel.setHgap(10);
        panel.setVgap(10);
        panel.setPadding(new Insets(20,20,20,20));
        
        Label lblTitulo = new Label("Los campos no validos seran ignorados");
        panel.add(lblTitulo, 0, 0);
        
        Label lblMatricula = new Label("Matricula");
        TextField txtMatricula = new TextField("");
        
        panel.add(lblMatricula, 0, 1);
        panel.add(txtMatricula, 1, 1);
        
        
        Button btnBuscar = new Button("Buscar");        
        panel.add(btnBuscar, 6, 1);
        btnBuscar.setOnAction(e->{
            String matricula = txtMatricula.getText();
            if (matricula.isEmpty()){
                VendedorController.showAlert("Error Formulario", "Matricula", "La matricula es invalida");
            } else {
                List<Oferta> ofertas = VendedorController.getOfertas(usuario, matricula);
                dibujarOfertas(ofertas);
            }
            //dibujarVehiculos(CompradorController.getVehiculos(tipoVehiculo, txtAnioInicio.getText(), txtAnioFin.getText(), txtPrecioInicio.getText(), txtPrecioFin.getText()));
        });
        
        root.setTop(panel);
        
        
        
        
    }

    
    
    private void dibujarOfertas(List<Oferta> ofertas){
        FlowPane contenedor = new FlowPane();
        contenedor.setStyle("-fx-background-color: #fbe2e7");
        //contenedor.setAlignment(Pos.CENTER);
        contenedor.setPadding(new Insets(10,10,10,10));
        contenedor.setHgap(10);
        contenedor.setVgap(10);

        //ArrayList<Producto> productos = Api.getProducts();

        for (Oferta p : ofertas){
            LayoutOferta ly = new LayoutOferta(p);
            contenedor.getChildren().add(ly);
        }

        for (Node nodo : contenedor.getChildren()){
            if (nodo instanceof LayoutOferta){
                nodo.setOnMouseClicked(e->{                    
                    //this.nextScene(root, new EnviarPujaVehiculoView(usuario, ((LayoutVehiculo) nodo).getProducto()) );
                    askBid(((LayoutOferta) nodo).getOferta());
                });
            }
        }

        ScrollPane panel = new ScrollPane();

        panel.setFitToWidth(true);
        panel.setFitToHeight(true);
        panel.setContent(contenedor);
        root.setCenter(panel);
    }
    
    private void askBid(Oferta oferta){
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Dialogo Puja");
        alert.setHeaderText("Aceptar la oferta de "+oferta.getComprador());
        alert.setContentText("Estas seguro?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            // ... user chose OK
            VendedorController.aceptarOferta(oferta);
            System.out.println("aceptaste");
            salir();
        } else {
            // ... user chose CANCEL or closed the dialog
            System.out.println("no aceptaste");
        }
    }
    
    @Override
    public Pane getRoot() {
        return root;
    }
    
    private void salir(){
        nextScene(root, new DashboardView(usuario));
    }

    
}
