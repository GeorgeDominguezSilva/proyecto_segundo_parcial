/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.gui;

import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
/**
 *
 * @author Usuario
 */
public abstract class PaneOrganizer {
    public abstract Pane getRoot();//get root
    
    public void nextScene(Pane current, PaneOrganizer next){
        //((Stage)current.getScene().getWindow()).setScene(new Scene(next.getRoot(), ScreenConfig.width, ScreenConfig.height));
        nextScene(current,next,ScreenConfig.width, ScreenConfig.height);
    }
    
    public void nextScene(Pane current, PaneOrganizer next, double width, double height){
        ((Stage)current.getScene().getWindow()).setScene(new Scene(next.getRoot(), width, height));
    }
    
    
}
