/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.gui.user.vendedor;

import ec.edu.espol.gui.InicioView;
import ec.edu.espol.gui.PaneOrganizer;
import ec.edu.espol.gui.user.ChangePasswordView;
import ec.edu.espol.gui.user.ChangeRoleView;
import ec.edu.espol.gui.user.DashboardView;
import ec.edu.espol.model.Persona;
import ec.edu.espol.model.personas.Agente;
import ec.edu.espol.model.personas.Comprador;
import ec.edu.espol.model.personas.Vendedor;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 *
 * @author Usuario
 */
public class NuevoVehiculoView extends PaneOrganizer {

    
    
    private BorderPane root;
    private Persona usuario;
    
    public NuevoVehiculoView(Persona persona){        
        root = new BorderPane();
        this.usuario = persona;
        cargarPanelSuperior();
        cargarPanelInferior();
        cargarPanelCentral();
    }
    
    //cargo el panel superior
    public void cargarPanelSuperior(){
        HBox panel = new HBox();
        panel.setPrefHeight(50);
        panel.setStyle("-fx-background-color: #000046;");
        root.setTop(panel);
    }    
    //cargo el panel inferior
    public void cargarPanelInferior(){
        HBox panel = new HBox();
        panel.setPrefHeight(50);
        panel.setStyle("-fx-background-color: #000046;");
        root.setBottom(panel);
    }
    
    public void cargarPanelCentral(){
        FlowPane contenedor = new FlowPane();
        contenedor.setAlignment(Pos.CENTER);
        contenedor.setPadding(new Insets(20,20,20,20));
        contenedor.setHgap(20);
        contenedor.setVgap(20);
        
        //contenedor.getChildren().addAll(btnEstudiantes,btnRecargas,btnSalir);

        //si es comprador
        
        LayoutOption lyAuto = new LayoutOption("Auto");//nuevo auto
        LayoutOption lyCamion = new LayoutOption("Camion");//nuevo auto
        LayoutOption lyCamioneta = new LayoutOption("Camioneta");//nuevo auto
        LayoutOption lyMotocicleta = new LayoutOption("Motocicleta");//nuevo auto
        
        LayoutOption lySalir = new LayoutOption("Salir");
        
        
        //acciones
        lySalir.setOnMouseClicked(e->salir());        
        
        lyAuto.setOnMouseClicked(e->{
            nextScene(root, new NuevoVehiculoFormView(usuario,"Auto"));
        });
        lyCamion.setOnMouseClicked(e->{
            nextScene(root, new NuevoVehiculoFormView(usuario,"Camion"));
        });
        lyCamioneta.setOnMouseClicked(e->{
            nextScene(root, new NuevoVehiculoFormView(usuario,"Camioneta"));
        });
        lyMotocicleta.setOnMouseClicked(e->{
            nextScene(root, new NuevoVehiculoFormView(usuario,"Motocicleta"));
        });
        
        //contenedor.getChildren().addAll(lyOfertar,lyNuevo,lyAceptar,lyRol,lySeguridad,lySalir);
        contenedor.getChildren().addAll(lyAuto, lyCamion, lyCamioneta, lyMotocicleta, lySalir);

        root.setCenter(contenedor);
    }
    
    private void salir(){
        nextScene(root, new DashboardView(usuario));
    }


    @Override
    public Pane getRoot() {return root;}
    
}
