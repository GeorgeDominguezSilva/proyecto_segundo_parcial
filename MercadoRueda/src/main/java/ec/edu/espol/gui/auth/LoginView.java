/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.gui.auth;

import ec.edu.espol.controller.AuthController;
import ec.edu.espol.gui.InicioView;
import ec.edu.espol.gui.PaneOrganizer;
import ec.edu.espol.model.Persona;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

//ejemplo youtube
public class LoginView extends PaneOrganizer{

    private BorderPane root;
    private HBox panelSuperior;
    private GridPane panelCentral;
    
    public LoginView(){
        root = new BorderPane();
        root.getStylesheets().add("styles/Auth.css");
        //panel superior
        panelSuperior = new HBox();                
        //panel central
        panelCentral = new GridPane();
        
        //load components
        cargarPanelSuperior();
        cargarPanelCentral();
        
        //root loading
        root.setTop(panelSuperior);
        root.setCenter(panelCentral);
    }
    
    void cargarPanelSuperior(){
        panelSuperior.setAlignment(Pos.BASELINE_CENTER);
        panelSuperior.setSpacing(20);
        panelSuperior.setPadding(new Insets(20,20,20,20));
        
        Label lblTitulo = new Label("Iniciar Sesion");
        lblTitulo.getStyleClass().add("titulo");
        panelSuperior.getChildren().add(lblTitulo);
    }
    
    void cargarPanelCentral(){
        panelCentral.setAlignment(Pos.CENTER);
        panelCentral.setHgap(20);
        panelCentral.setVgap(20);
        
        Label lblUser = new Label("Usuario");
        Label lblPassword = new Label("Clave: ");
        TextField txtUser = new TextField();
        PasswordField txtPassword = new PasswordField();
        txtUser.setText("");
        txtPassword.setText("");
        panelCentral.add(lblUser, 0, 0); //pane,col,row
        panelCentral.add(txtUser, 1, 0); //pane,col,row        
        panelCentral.add(lblPassword, 0, 1); //pane,col,row
        panelCentral.add(txtPassword, 1, 1); //pane,col,row
        
        Button btnIngresar = new Button("Ingresar");
        btnIngresar.setOnAction(e->{AuthController.auth(txtUser.getText(), txtPassword.getText(), this);});
        Button btnSalir = new Button("Regresar");
        btnSalir.getStyleClass().add("btn-salir");        
        btnSalir.setOnAction(e->nextScene(root,new InicioView()));
        panelCentral.add(btnIngresar, 1, 2); //pane,col,row
        panelCentral.add(btnSalir, 1, 3); //pane,col,row
        
    }
    
    @Override
    public Pane getRoot() {
        return root;
    }
    
    /*    
    void enviarFormulario(String user, String password){
    
        if ( user.isEmpty() || password.isEmpty() ){
            alertaDanger();
        } else {
            Persona persona = AuthController.login(user, password);
            if ( persona!=null ){
                alertaSuccess();
                this.nextScene(root, new InicioView());
            } else {
                alertaDanger();
            }
        }
    }
    
    void alertaDanger(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Informacion");
        alert.setHeaderText("Datos de Formulario no validos");
        alert.setContentText("El usuario no existe, o los campos son invalidos");
        alert.showAndWait();
    }
    
    void alertaSuccess(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Informacion");
        alert.setHeaderText("Autorizacion Exitosa");
        alert.setContentText("Has inicado sesion");
        alert.showAndWait();
    }
    */
    
}
