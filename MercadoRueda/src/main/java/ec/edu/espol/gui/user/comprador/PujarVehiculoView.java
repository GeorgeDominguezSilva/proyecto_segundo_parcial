/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.gui.user.comprador;

import ec.edu.espol.controller.CompradorController;
import ec.edu.espol.gui.PaneOrganizer;
import ec.edu.espol.gui.user.DashboardView;
import ec.edu.espol.model.Persona;
import ec.edu.espol.model.Vehiculo;
import ec.edu.espol.model.personas.Comprador;
import ec.edu.espol.model.personas.Vendedor;
import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Usuario
 */
public class PujarVehiculoView extends PaneOrganizer{
    
    private BorderPane root;
    private Persona usuario;
    
    public PujarVehiculoView(Persona persona){
        root = new BorderPane();        
        this.usuario = persona;
        root.getStylesheets().add("styles/PujarVehiculo.css");
        
        cargarPanelSuperior();
        
        cargarPanelInferior();
        
        //opcional
        dibujarVehiculos(CompradorController.getVehiculos("", "", "", "", ""));
    }
    
    private void cargarPanelInferior(){
        BorderPane panel = new BorderPane();
        panel.setStyle("-fx-background-color: #000046");
        Button btnSalir = new Button("Salir");
        btnSalir.getStyleClass().add("btn-salir");
        btnSalir.setPrefWidth(220);
                
        panel.setLeft(btnSalir);
        
        
        
        root.setBottom(panel);
        btnSalir.setOnAction(e->salir());
        
    }

    
    private void cargarPanelSuperior(){
        GridPane panel = new GridPane();
        panel.setAlignment(Pos.CENTER);
        panel.setHgap(10);
        panel.setVgap(10);
        panel.setPadding(new Insets(20,20,20,20));
        
        Label lblTitulo = new Label("Los campos no validos seran ignorados");
        panel.add(lblTitulo, 0, 0);
        
        Label lblAnioInicio = new Label("Anio Inicio:");
        TextField txtAnioInicio = new TextField("");
        panel.add(lblAnioInicio, 0, 1);
        panel.add(txtAnioInicio, 1, 1);
        
        Label lblAnioFin = new Label("Anio Fin:");
        TextField txtAnioFin = new TextField("");
        panel.add(lblAnioFin, 3, 1);
        panel.add(txtAnioFin, 4, 1);
        
        Label lblPrecioInicio = new Label("Precio Inicio:");
        TextField txtPrecioInicio = new TextField("");
        panel.add(lblPrecioInicio, 0, 2);
        panel.add(txtPrecioInicio, 1, 2);
        
        Label lblPrecioFin = new Label("Precio Fin:");
        TextField txtPrecioFin = new TextField("");
        panel.add(lblPrecioFin, 3, 2);
        panel.add(txtPrecioFin, 4, 2);
        
        final ToggleGroup group = new ToggleGroup();
        RadioButton rbTodos = new RadioButton("Todos");
        RadioButton rbAuto = new RadioButton("Auto");
        RadioButton rbCamion = new RadioButton("Camion");
        RadioButton rbCamioneta = new RadioButton("Camioneta");
        RadioButton rbMotocicleta = new RadioButton("Motocicleta");        
        rbTodos.setToggleGroup(group);
        rbTodos.setSelected(true);
        rbAuto.setToggleGroup(group);
        rbCamion.setToggleGroup(group);
        rbCamioneta.setToggleGroup(group);
        rbMotocicleta.setToggleGroup(group);
        
        VBox containerRadios = new VBox();
        containerRadios.getChildren().addAll(rbTodos,rbAuto, rbCamion, rbCamioneta, rbMotocicleta);
        containerRadios.setAlignment(Pos.CENTER_LEFT);
        containerRadios.setSpacing(5);
        
        
        panel.add(new Label("Tipo"), 0 , 3 );
        panel.add(containerRadios, 1 , 3 );
        
        
        Button btnBuscar = new Button("Buscar");        
        panel.add(btnBuscar, 6, 1);
        btnBuscar.setOnAction(e->{
            String tipoVehiculo = ((RadioButton)group.getSelectedToggle()).getText();
            if (tipoVehiculo.equals("Todos")){
                tipoVehiculo = "";
            }
            dibujarVehiculos(CompradorController.getVehiculos(tipoVehiculo, txtAnioInicio.getText(), txtAnioFin.getText(), txtPrecioInicio.getText(), txtPrecioFin.getText()));
        });
        
        root.setTop(panel);
        
        
        
        
    }

    
    
    private void dibujarVehiculos(List<Vehiculo> productos){
        FlowPane contenedor = new FlowPane();
        contenedor.setStyle("-fx-background-color: #fbe2e7");
        //contenedor.setAlignment(Pos.CENTER);
        contenedor.setPadding(new Insets(10,10,10,10));
        contenedor.setHgap(10);
        contenedor.setVgap(10);

        //ArrayList<Producto> productos = Api.getProducts();

        for (Vehiculo p : productos){
            LayoutVehiculo ly = new LayoutVehiculo(p);
            contenedor.getChildren().add(ly);
        }

        for (Node nodo : contenedor.getChildren()){
            if (nodo instanceof LayoutVehiculo){
                nodo.setOnMouseClicked(e->{
                    //lyCarrito.agregarItem(((LayoutProducto) nodo).getProducto());                    
                    this.nextScene(root, new EnviarPujaVehiculoView(usuario, ((LayoutVehiculo) nodo).getProducto()) );
                });
            }
        }

        ScrollPane panel = new ScrollPane();

        panel.setFitToWidth(true);
        panel.setFitToHeight(true);
        panel.setContent(contenedor);
        root.setCenter(panel);
    }
    
    @Override
    public Pane getRoot() {
        return root;
    }
    
    private void salir(){
        nextScene(root, new DashboardView(usuario));
    }

    
}
