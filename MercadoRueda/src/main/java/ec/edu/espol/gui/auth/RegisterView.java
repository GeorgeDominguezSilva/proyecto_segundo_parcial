/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.gui.auth;

import ec.edu.espol.controller.AuthController;
import ec.edu.espol.gui.InicioView;
import ec.edu.espol.gui.PaneOrganizer;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

//ejemplo youtube
public class RegisterView extends PaneOrganizer{

    private BorderPane root;
    private HBox panelSuperior;
    private GridPane panelCentral;
    
    public RegisterView(){
        root = new BorderPane();
        root.getStylesheets().add("styles/Auth.css");
        
        //panel superior
        panelSuperior = new HBox();                
        //panel central
        panelCentral = new GridPane();
        
        //load components
        cargarPanelSuperior();
        cargarPanelCentral();
        
        //root loading
        root.setTop(panelSuperior);
        root.setCenter(panelCentral);
    }
    
    void cargarPanelSuperior(){
        panelSuperior.setAlignment(Pos.BASELINE_CENTER);
        panelSuperior.setSpacing(20);
        panelSuperior.setPadding(new Insets(20,20,20,20));
        
        Label lblTitulo = new Label("Registrarse");
        lblTitulo.getStyleClass().add("titulo");
        panelSuperior.getChildren().add(lblTitulo);
    }
    
    void cargarPanelCentral(){
        panelCentral.setAlignment(Pos.CENTER);
        panelCentral.setHgap(20);
        panelCentral.setVgap(20);
        
        final ToggleGroup group = new ToggleGroup();
        RadioButton rbVendedor = new RadioButton("Vendedor");
        RadioButton rbComprador = new RadioButton("Comprador");
        RadioButton rbAgente = new RadioButton("Agente");
        rbComprador.setToggleGroup(group);
        rbComprador.setSelected(true);
        rbVendedor.setToggleGroup(group);
        rbAgente.setToggleGroup(group);
        VBox containerRadios = new VBox();
        containerRadios.getChildren().addAll(rbComprador,rbVendedor,rbAgente);
        containerRadios.setAlignment(Pos.CENTER_LEFT);
        containerRadios.setSpacing(10);
        
        Label lblTipo = new Label("Tipo Cuenta");
        Label lblNombre = new Label("Nombre");
        Label lblApellido = new Label("Apellido");
        Label lblCorreo = new Label("Correo");
        Label lblOrganizacion = new Label("Organizacion");
        Label lblUser = new Label("Usuario");
        Label lblPassword = new Label("Clave: ");
        TextField txtNombre = new TextField("");
        TextField txtApellido = new TextField("");
        TextField txtCorreo = new TextField("");
        TextField txtOrganizacion = new TextField("");        
        TextField txtUser = new TextField();
        PasswordField txtPassword = new PasswordField();
        
        panelCentral.add(lblTipo, 0, 0);
        panelCentral.add(containerRadios, 1, 0);
        
        panelCentral.add(lblNombre, 0, 1);
        panelCentral.add(txtNombre, 1, 1);
        
        panelCentral.add(lblApellido, 0, 2);
        panelCentral.add(txtApellido, 1, 2);
        
        panelCentral.add(lblCorreo, 0, 3);
        panelCentral.add(txtCorreo, 1, 3);
        
        panelCentral.add(lblOrganizacion, 0, 4);
        panelCentral.add(txtOrganizacion, 1, 4);
        
        panelCentral.add(lblUser, 0, 5); //pane,col,row
        panelCentral.add(txtUser, 1, 5); //pane,col,row        
        panelCentral.add(lblPassword, 0, 6); //pane,col,row
        panelCentral.add(txtPassword, 1, 6); //pane,col,row
        
        
        
        Button btnIngresar = new Button("Registro");
        btnIngresar.setOnAction(e->{
            
            enviarFormulario(
                    ((RadioButton)group.getSelectedToggle()).getText(),
                    txtNombre.getText(),txtApellido.getText(),txtCorreo.getText(),
                    txtOrganizacion.getText(), txtUser.getText(), txtPassword.getText()
            );
            
        });
        Button btnSalir = new Button("Regresar");
        btnSalir.getStyleClass().add("btn-salir");
        btnSalir.setOnAction(e->nextScene(root,new InicioView()));
        panelCentral.add(btnIngresar, 1, 7); //pane,col,row
        panelCentral.add(btnSalir, 1, 8); //pane,col,row
        
    }
    
    @Override
    public Pane getRoot() {
        return root;
    }
    
    
    void enviarFormulario(String tipo, String nombre, String apellido, String correo, String organizacion, String user, String password){
    
        if (nombre.isEmpty() || apellido.isEmpty() || correo.isEmpty() ||
            organizacion.isEmpty() || user.isEmpty() || password.isEmpty() ||
                !AuthController.isValidEmail(correo)
            ){
            alertaDanger();
        } else {
            boolean response = AuthController.registro(tipo, nombre, apellido, correo, organizacion, user, password);
            if (response){
                alertaSuccess();
                this.nextScene(root, new InicioView());
            } else {
                alertaDanger();
            }
        }
    }
    
    void alertaDanger(){
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Informacion");
        alert.setHeaderText("Datos de Formulario no validos");
        alert.setContentText("El usuario ya existe, o los campos no son invalidos");
        alert.showAndWait();
    }
    
    void alertaSuccess(){
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Informacion");
        alert.setHeaderText("Has creado un nuevo usuario");
        alert.setContentText("Registro exitoso, pronto recibiras un email de bienvenida");
        alert.showAndWait();
    }
    
}
