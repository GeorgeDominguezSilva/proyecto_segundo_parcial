/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.gui.user.vendedor;

/**
 *
 * @author Usuario
 */
import ec.edu.espol.gui.user.*;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;


public class LayoutOption extends VBox {
    //private VBox root;

    public LayoutOption(String titulo){

        //root = new VBox();
        //root.setPrefSize(100,100);
        this.getStylesheets().add("styles/LayoutOption.css");

        this.setPrefSize(150,150);
        this.setAlignment(Pos.CENTER);
        this.setSpacing(10);
        Label lblTitulo = new Label(titulo);

        ImageView iv = new ImageView();
        iv.setFitWidth(100);
        iv.setFitHeight(100);
        //iv.setPreserveRatio(true);

        try{
            Image im = new Image("/img/vehiculos/"+titulo+".png");
            iv.setImage(im);
        } catch (Exception ex){
            Image imr = new Image("/img/error.png");
            iv.setImage(imr);
            //System.err.println("no se puso leer la imagen para el layout");
        }
        this.getChildren().add(iv);

        this.getChildren().add(lblTitulo);
    }

}

