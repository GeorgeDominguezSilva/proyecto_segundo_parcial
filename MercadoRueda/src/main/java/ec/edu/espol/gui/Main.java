/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.gui;

import ec.edu.espol.services.EmailService;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Usuario
 */
public class Main extends Application {
    
    /*
    @Override
    public void start(Stage stage) {
        var javaVersion = SystemInfo.javaVersion();
        var javafxVersion = SystemInfo.javafxVersion();

        var label = new Label("Hello, JavaFX " + javafxVersion + ", running on Java " + javaVersion + ".");
        var scene = new Scene(new StackPane(label), 640, 480);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
    */
    
    @Override
    public void start(Stage primaryStage) {
                
        
        primaryStage.setTitle("MercadoRueda.com");
        primaryStage.setScene(new Scene(new InicioView().getRoot(), ScreenConfig.width, ScreenConfig.height));
        
        primaryStage.setOnCloseRequest(e->{
            System.exit(0);
        });//por si dejamos hilos abiertos

        primaryStage.setResizable(false);
        primaryStage.show();
        EmailService.readProperties();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}