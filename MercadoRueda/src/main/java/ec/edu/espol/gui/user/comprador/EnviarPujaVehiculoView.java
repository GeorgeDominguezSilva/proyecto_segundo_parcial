/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.gui.user.comprador;

import ec.edu.espol.controller.CompradorController;
import ec.edu.espol.gui.PaneOrganizer;
import ec.edu.espol.gui.user.DashboardView;
import ec.edu.espol.model.Persona;
import ec.edu.espol.model.Vehiculo;
import ec.edu.espol.model.vehiculos.Motocicleta;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Usuario
 */
public class EnviarPujaVehiculoView extends PaneOrganizer{
    
    private VBox root;
    private GridPane panelCentral;
    
    private Persona usuario;
    private Vehiculo vehiculo;

    public EnviarPujaVehiculoView(Persona persona, Vehiculo vehiculo) {
        this.root = new VBox();
        root.getStylesheets().add("styles/PujarVehiculo.css");
        root.setSpacing(10);
        
        this.panelCentral = new GridPane();
        this.usuario = persona;
        this.vehiculo = vehiculo;        
        root.setAlignment(Pos.CENTER);
        
        /*
        Label lblPrecio = new Label("Precio a ofertar");
        TextField txtPrecio = new TextField("0");
        txtPrecio.setMaxWidth(200);
        Button btnEnviar = new Button("Enviar");
        Button btnSalir = new Button("Cancelar");
        btnSalir.getStyleClass().add("btn-salir");
        btnSalir.setOnAction(e->{
            salir();
        });
        btnEnviar.setOnAction(e->{
            double precio = 0;
            try{
                precio = Double.parseDouble(txtPrecio.getText());
            } catch (NumberFormatException ex){
                CompradorController.showAlert("Formulario", "Precio", "El precio debe ser decimal. Ejm: 1000.00");
                return;
            }
            if (CompradorController.pujar(this.vehiculo, this.usuario, precio)){
                CompradorController.showAlert("Formulario", "Puja", "Puja Exitosa");
                salir();
            }else {
                CompradorController.showAlert("Formulario", "Puja", "Puja Fallida, intenta con otro monto");
            }
        });
        root.getChildren().addAll(lblPrecio,txtPrecio,btnEnviar,btnSalir);
        */
        root.getChildren().add(panelCentral);
        cargarPanelCentral();
    }
    
    void cargarPanelCentral(){
        panelCentral.setAlignment(Pos.CENTER);
        panelCentral.setHgap(20);
        panelCentral.setVgap(10);
        
        //public Vehiculo(String vendedor, String placa, String marca, String modelo,
        //String tipoMotor, int anio, double recorrido, String color,
        //String tipoCombustible, double precio, String transmision, int vidrios) {
        Label lblPlaca = new Label("Placa");
        TextField txtPlaca = new TextField(vehiculo.getPlaca());
        txtPlaca.setDisable(true);
        
        Label lblMarca = new Label("Marca");
        TextField txtMarca = new TextField(vehiculo.getMarca());
        txtMarca.setDisable(true);
        
        Label lblModelo = new Label("Modelo");
        TextField txtModelo = new TextField(vehiculo.getModelo());        
        txtModelo.setDisable(true);
        
        Label lblTipoMotor = new Label("Tipo Motor");
        TextField txtTipoMotor = new TextField(vehiculo.getTipoMotor());
        txtTipoMotor.setDisable(true);
        
        Label lblAnio = new Label("Anio"); //validar
        TextField txtAnio = new TextField(""+vehiculo.getAnio());
        txtAnio.setDisable(true);
        
        Label lblRecorrido = new Label("Recorrido"); //validar
        TextField txtRecorrido = new TextField(""+vehiculo.getRecorrido());
        txtRecorrido.setDisable(true);
        
        Label lblColor = new Label("Color");
        TextField txtColor = new TextField(vehiculo.getColor());
        txtColor.setDisable(true);
        
        Label lblTipoCombustible = new Label("Tipo Combustible");
        TextField txtTipoCombustible = new TextField(vehiculo.getTipoCombustible());
        txtTipoCombustible.setDisable(true);
        
        Label lblPrecio = new Label("Precio: $"); //validar
        TextField txtPrecio = new TextField(""+vehiculo.getPrecio());
        
        Label lblTransmision = new Label("Transmision");
        TextField txtTransmision = new TextField(vehiculo.getTransmision());
        txtTransmision.setDisable(true);
        
        Label lblVidrios = new Label("Vidrios"); //validar
        TextField txtVidrios = new TextField(""+vehiculo.getVidrios());
        txtVidrios.setDisable(true);
        
        if (vehiculo instanceof Motocicleta){
            lblVidrios.setVisible(false);
            txtVidrios.setVisible(false);
        }
        
        //Label lblCurrentPassword = new Label("Clave");
        //Label lblNewPassword = new Label("Nueva: ");
        //PasswordField txtCurrentPassword = new PasswordField();
        //PasswordField txtNewPassword = new PasswordField();
        
        panelCentral.add(lblPlaca, 0, 0); //pane,col,row
        panelCentral.add(txtPlaca, 1, 0); //pane,col,row        
        
        panelCentral.add(lblMarca, 0, 1); //pane,col,row
        panelCentral.add(txtMarca, 1, 1); //pane,col,row        
        
        panelCentral.add(lblModelo, 0, 2); //pane,col,row
        panelCentral.add(txtModelo, 1, 2); //pane,col,row 
        
                
        panelCentral.add(lblTipoMotor, 0, 3); //pane,col,row
        panelCentral.add(txtTipoMotor, 1, 3); //pane,col,row 
        
        panelCentral.add(lblAnio, 0, 4); //pane,col,row
        panelCentral.add(txtAnio, 1, 4); //pane,col,row 
        
        panelCentral.add(lblRecorrido, 0, 5); //pane,col,row
        panelCentral.add(txtRecorrido, 1, 5); //pane,col,row 
        
        panelCentral.add(lblColor, 0, 6); //pane,col,row
        panelCentral.add(txtColor, 1, 6); //pane,col,row 
        
        panelCentral.add(lblTipoCombustible, 0, 7); //pane,col,row
        panelCentral.add(txtTipoCombustible, 1, 7); //pane,col,row
        
        panelCentral.add(lblPrecio, 0, 8); //pane,col,row
        panelCentral.add(txtPrecio, 1, 8); //pane,col,row
        
        panelCentral.add(lblTransmision, 0, 9); //pane,col,row
        panelCentral.add(txtTransmision, 1, 9); //pane,col,row
        
        panelCentral.add(lblVidrios, 0, 10); //pane,col,row
        panelCentral.add(txtVidrios, 1, 10); //pane,col,row
        
        
        
        
        
        Button btnCrear = new Button("Enviar");
        btnCrear.setOnAction(e->{
            double precio = 0;
            try{
                precio = Double.parseDouble(txtPrecio.getText());
            } catch (NumberFormatException ex){
                CompradorController.showAlert("Formulario", "Precio", "El precio debe ser decimal. Ejm: 1000.00");
                return;
            }
            if (CompradorController.pujar(this.vehiculo, this.usuario, precio)){
                CompradorController.showAlert("Formulario", "Puja", "Puja Exitosa");
                salir();
            }else {
                CompradorController.showAlert("Formulario", "Puja", "Puja Fallida, intenta con otro monto");
            }
        });
        
        Button btnSalir =    new Button("Regresar");
        btnSalir.getStyleClass().add("btn-salir");        
        btnSalir.setOnAction(e->nextScene(root, new DashboardView(usuario) ));
        panelCentral.add(btnCrear, 4, 0); //pane,col,row
        panelCentral.add(btnSalir, 4, 1); //pane,col,row
        
    }

    @Override
    public Pane getRoot() {
        return root;
    }
    
    private void salir(){
        nextScene(root, new DashboardView(usuario));
    }
    
    
    
}
