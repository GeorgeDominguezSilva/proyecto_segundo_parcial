/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.gui.user;

import ec.edu.espol.controller.AuthController;
import ec.edu.espol.gui.InicioView;
import ec.edu.espol.gui.PaneOrganizer;
import ec.edu.espol.gui.user.comprador.PujarVehiculoView;
import ec.edu.espol.gui.user.vendedor.AceptarOfertaVehiculoView;
import ec.edu.espol.gui.user.vendedor.NuevoVehiculoView;
import ec.edu.espol.model.Persona;
import ec.edu.espol.model.personas.Agente;
import ec.edu.espol.model.personas.Comprador;
import ec.edu.espol.model.personas.Vendedor;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Usuario
 */
public class DashboardView extends PaneOrganizer {
    
    private BorderPane root;
    private Persona usuario;
    
    public DashboardView(Persona persona){        
        root = new BorderPane();
        this.usuario = persona;
        cargarPanelSuperior();
        cargarPanelInferior();
        cargarPanelCentral();
        cargarPanelLateral();
    }
    
    //cargo el panel superior
    public void cargarPanelSuperior(){
        HBox panel = new HBox();
        panel.setPrefHeight(50);
        panel.setStyle("-fx-background-color: #000046;");
        root.setTop(panel);
    }    
    //cargo el panel inferior
    public void cargarPanelInferior(){
        HBox panel = new HBox();
        panel.setPrefHeight(50);
        panel.setStyle("-fx-background-color: #000046;");
        root.setBottom(panel);
    }
    
    public void cargarPanelCentral(){
        FlowPane contenedor = new FlowPane();
        contenedor.setAlignment(Pos.CENTER);
        contenedor.setPadding(new Insets(20,20,20,20));
        contenedor.setHgap(20);
        contenedor.setVgap(20);
        
        //contenedor.getChildren().addAll(btnEstudiantes,btnRecargas,btnSalir);

        //si es comprador
        LayoutOption lyOfertar = new LayoutOption("Ofertar");//oferta por un vehiculo
        //si es vendedor
        LayoutOption lyNuevo = new LayoutOption("Nuevo");//nuevo vehiculo
        LayoutOption lyAceptar = new LayoutOption("Aceptar");//aceptar ofertas
        //agente
        //todas las anteriores
        
        //todos
        LayoutOption lyRol = new LayoutOption("Rol");//contrasena y rol
        LayoutOption lySeguridad = new LayoutOption("Seguridad");//contrasena y rol        
        LayoutOption lySalir = new LayoutOption("Salir");

        //acciones
        lySalir.setOnMouseClicked(e->salir());        
        lySeguridad.setOnMouseClicked(e->{
            this.nextScene(root, new ChangePasswordView(usuario));
        });
        lyRol.setOnMouseClicked(e->{
            this.nextScene(root, new ChangeRoleView(usuario));
        });
        
        lyNuevo.setOnMouseClicked(e->{
            this.nextScene(root, new NuevoVehiculoView(usuario));
        });
        
        lyOfertar.setOnMouseClicked(e->{
            this.nextScene(root, new PujarVehiculoView(usuario));
        });
        
        lyAceptar.setOnMouseClicked(e->{
            this.nextScene(root, new AceptarOfertaVehiculoView(usuario));
        });
        

        if(usuario instanceof Comprador || usuario instanceof Agente){
            contenedor.getChildren().add(lyOfertar);
        }
        
        if(usuario instanceof Vendedor || usuario instanceof Agente){
            contenedor.getChildren().addAll(lyNuevo,lyAceptar);
        }
        
        //contenedor.getChildren().addAll(lyOfertar,lyNuevo,lyAceptar,lyRol,lySeguridad,lySalir);
        contenedor.getChildren().addAll(lyRol,lySeguridad,lySalir);

        root.setCenter(contenedor);
    }
    
    private void cargarPanelLateral(){
        VBox profile = new VBox();
        profile.setAlignment(Pos.CENTER);
        profile.setSpacing(10);
        profile.setPadding(new Insets(15,15,15,15));
        
        profile.getStylesheets().add("styles/Profile.css");
        //btnSalir.getStyleClass().add("btn-salir");
        Label lblRol = new Label("Rol");               
        Label lblNombre = new Label("Nombre");
        Label lblApellido = new Label("Apellido");
        Label lblCorreo = new Label("Correo");
        Label lblOrganizacion = new Label("Organizacion");
        Label lblUsuario = new Label("Usuario");
        
        Label txtRol = new Label(parseText(usuario.getClass().getSimpleName()));
        txtRol.getStyleClass().add("subtitulo");
        
        Label txtNombre = new Label(parseText(usuario.getNombre()));
        txtNombre.getStyleClass().add("subtitulo");
        
        Label txtApellido = new Label(parseText(usuario.getApellido()));
        txtApellido.getStyleClass().add("subtitulo");
        
        Label txtCorreo = new Label(parseText(usuario.getCorreo()));
        txtCorreo.getStyleClass().add("subtitulo");
        
        Label txtOrganizacion = new Label(parseText(usuario.getOrganizacion()));
        txtOrganizacion.getStyleClass().add("subtitulo");
        
        Label txtUsuario = new Label(parseText(usuario.getUsuario()));
        txtUsuario.getStyleClass().add("subtitulo");
        
        profile.getChildren().add(lblRol);
        profile.getChildren().add(txtRol);
        
        
        profile.getChildren().add(lblUsuario);
        profile.getChildren().add(txtUsuario);
        
        profile.getChildren().add(lblCorreo);
        profile.getChildren().add(txtCorreo);
        
        profile.getChildren().add(lblNombre);
        profile.getChildren().add(txtNombre);
        
        profile.getChildren().add(lblApellido);
        profile.getChildren().add(txtApellido);
        
        
        
        profile.getChildren().add(lblOrganizacion);
        profile.getChildren().add(txtOrganizacion);
        
        
        
        
        
        root.setRight(profile);
    }
    
    private void salir(){
        nextScene(root, new InicioView());
    }
    
    private String parseText(String inputText){
        int limit = 20;
        if (inputText.isEmpty()){
            return "...";
        }
        if (inputText.length()>limit){
            return inputText.substring(0, limit)+"...";
        }
        return inputText;
    }

    
    



    @Override
    public Pane getRoot() {return root;}
    
}
