/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.PaneOrganizer;
import ec.edu.espol.gui.user.DashboardView;
import ec.edu.espol.model.Oferta;
import ec.edu.espol.model.Persona;
import ec.edu.espol.model.Vehiculo;
import ec.edu.espol.model.vehiculos.Auto;
import ec.edu.espol.model.vehiculos.Camion;
import ec.edu.espol.model.vehiculos.Camioneta;
import ec.edu.espol.model.vehiculos.Motocicleta;
import ec.edu.espol.services.api.ApiOferta;
import ec.edu.espol.services.api.ApiVehiculo;
import java.util.List;
import javafx.scene.control.Alert;

/**
 *
 * @author Usuario
 */
public class VendedorController {
    
    
    public static void aceptarOferta(Oferta oferta){
        if (ApiOferta.aceptarOferta(oferta)){            
            System.out.println("Vendido exitosamente, contacta al vendedor para concretar: "+oferta.getComprador());
        }   else{
            System.err.println("No se pudo completar la compra, contacta al administrador");
        }     
    }
    
    public static List<Oferta>  getOfertas(Persona vendedor, String matricula){
        List<Oferta> response = ApiOferta.getOfertas(matricula,vendedor);
        return response;
    }
    
    //public Vehiculo(String vendedor, String placa, String marca, String modelo,
    //String tipoMotor, int anio, double recorrido, String color,
    //String tipoCombustible, double precio, String transmision, int vidrios) {
    
    public static void crearVehiculo(String tipo, Persona vendedor, String placa, String marca, String modelo, String tipoMotor, int anio, double recorrido, String color, String tipoCombustible, double precio, String transmision, int vidrios, PaneOrganizer view){
        Vehiculo vehiculo = new Vehiculo();
        switch (tipo){
            case "Auto":
                vehiculo = new Auto();
                break;
            case "Camion":
                vehiculo = new Camion();
                break;
            case "Camioneta":
                vehiculo = new Camioneta();
                break;
            case "Motocicleta":
                vehiculo = new Motocicleta();
                break;
        }
        
        //bindear data
        vehiculo.setPlaca(placa);
        vehiculo.setMarca(marca);
        vehiculo.setModelo(modelo);        
        vehiculo.setTipoMotor(tipoMotor);
        vehiculo.setAnio(anio);
        vehiculo.setRecorrido(recorrido);
        vehiculo.setColor(color);        
        vehiculo.setTipoCombustible(tipoCombustible);        
        vehiculo.setPrecio(precio);
        vehiculo.setTransmision(transmision);
        //exclusion
        if (!(vehiculo instanceof Motocicleta)){
            vehiculo.setVidrios(vidrios);
        } else {
            vehiculo.setVidrios(0);
        }
        //set dato del vendedor
        vehiculo.setVendedor(vendedor.getCorreo());
        
        if (ApiVehiculo.addVehiculo(vehiculo)){
            showAlert("Nuevo Vehiculo","Guardado","Guardado exitoso");
            view.nextScene(view.getRoot(), new DashboardView(vendedor));
        } else {
            showAlert("Nuevo Vehiculo","No Guardado","Verifique los Datos");
        }
    }
    
    public static void showAlert(String title, String header, String content){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.showAndWait();
    }
    
}
