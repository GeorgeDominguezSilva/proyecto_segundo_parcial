/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.InicioView;
import ec.edu.espol.gui.PaneOrganizer;
import ec.edu.espol.gui.user.DashboardView;
import ec.edu.espol.model.Persona;
import ec.edu.espol.model.personas.Agente;
import ec.edu.espol.model.personas.Comprador;
import ec.edu.espol.model.personas.Vendedor;
import ec.edu.espol.services.Api;
import ec.edu.espol.services.EmailService;
import ec.edu.espol.services.HashService;
import ec.edu.espol.services.api.ApiPersona;
import javafx.scene.control.Alert;


/**
 *
 * @author Usuario
 */
public class AuthController {
    
    public static void changeRole(Persona persona, String tipo, String password, PaneOrganizer view){
        //registro
        //boolean response = false;
        if (password.isEmpty() || !HashService.getHash(password).equals(persona.getPasswordHash())){
            alertaChangeRoleDanger();
            return;
        }
        
        Persona p = new Persona(); //mala practica pero como es un controller no importa siempre nos aseguramos del valor
        
        switch(tipo){
            case "Comprador":
                p = new Comprador(persona);
                break;
            case "Vendedor":
                p = new Vendedor(persona);
                break;
            case "Agente":
                p = new Agente(persona);
                break;
        }
        boolean response = ApiPersona.update(p);
        if (response){
            alertaChangeRoleSuccess();
            view.nextScene(view.getRoot(), new InicioView());
        } else {
            alertaChangeRoleDanger();
        }
        
    }
    
    public static void changePassword(Persona persona, String currentPassword, String newPassword, PaneOrganizer view){                
        if (currentPassword.isEmpty() || newPassword.isEmpty() /*|| currentPassword.equals(newPassword)*/){
            alertaChangePasswordDanger();
        } else {            
            String currentHash = HashService.getHash(currentPassword);
            String newHash = HashService.getHash(newPassword);
            if (
                    !currentHash.equals(persona.getPasswordHash()) ||
                    newHash.equals(persona.getPasswordHash())
               ){ //valido que no sea la misma current password en base
                alertaChangePasswordDanger();
                return;
            }            
            persona.setPasswordHash(newHash);            
            boolean response = ApiPersona.update(persona);
            if (response){
                alertaChangePasswordSuccess();
                view.nextScene(view.getRoot(), new InicioView());
            } else {
                alertaChangePasswordDanger();
            } 
            
        }
    }
    
    
    
    public static void auth (String user, String password, PaneOrganizer view){
        //aqui deberia cargar los datos y pasarle el user
        Persona persona = login( user, password );
        if (persona!=null){
            if (persona instanceof Vendedor){
                System.out.println("Un vendedor ha iniciado sesion");
            }
            if (persona instanceof Comprador){
                System.out.println("Un comprador ha iniciado sesion");
            }
            if (persona instanceof Agente){
                System.out.println("Un Agente ha iniciado sesion");
            }
            
            alertaLoginSuccess();
            view.nextScene(view.getRoot(), new DashboardView(persona));
        } else {
            alertaLoginDanger();
        }
    }
    
    private static Persona login(String usuario, String clave){
        //registro
        Persona persona = ApiPersona.login(usuario, clave);
        return persona;
    }
    
    public static boolean registro(String tipo, String nombre, String apellido, String correo, String organizacion, String usuario, String clave){
        //registro
        boolean response = false;
        switch(tipo){
            case "Comprador":
                Comprador comprador = new Comprador(nombre,apellido,correo,organizacion,usuario,clave);
                if (Api.addPersona(comprador)){
                    System.out.println("Comprador Creado de Manera exitosa");
                    response = true;
                } else {
                    System.err.println("Comprador No Creado");
                }
                break;
            case "Vendedor":
                Vendedor vendedor = new Vendedor(nombre,apellido,correo,organizacion,usuario,clave);
                if (Api.addPersona(vendedor)){
                    System.out.println("Vendedor Creado de Manera exitosa");
                    response = true;
                } else {
                    System.err.println("Vendedor No Creado");
                }
                break;
            case "Agente":
                Agente agente = new Agente(nombre,apellido,correo,organizacion,usuario,clave);
                if (Api.addPersona(agente)){
                    System.out.println("Agente Creado de Manera exitosa");
                    response = true;
                } else {
                    System.err.println("Agente No Creado");
                }
                break;
        }
        return response;
    }
    
    public static boolean isValidEmail(String correo){
        return EmailService.isValidEmail(correo);
    }
    
    static void alertaLoginDanger(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Informacion");
        alert.setHeaderText("Datos de Formulario no validos");
        alert.setContentText("El usuario no existe, o los campos no son invalidos");
        alert.showAndWait();
    }
    
    static void alertaLoginSuccess(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Informacion");
        alert.setHeaderText("Autorizacion Exitosa");
        alert.setContentText("Has inicado sesion");
        alert.showAndWait();
    }
    
    //changePassword
    static void alertaChangePasswordDanger(){
        showAlert("Alerta de seguridad","No se pudo cambiar","Verifica los campos y que la nueva clave sea diferente");
    }    
    static void alertaChangePasswordSuccess(){
        showAlert("Alerta de seguridad","Actualizacion Exitosa","Has cambiado con exito la clave");
    }
    
    //changeRole
    static void alertaChangeRoleDanger(){
        showAlert("Alerta de seguridad","No se pudo cambiar tu Rol","Verifica la clave");
    }    
    static void alertaChangeRoleSuccess(){
        showAlert("Alerta de seguridad","Actualizacion de Rol Exitosa","Has cambiado con exito el rol de tu cuenta");
    }
    
    static void showAlert(String title, String header, String content){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.showAndWait();
    }
}
