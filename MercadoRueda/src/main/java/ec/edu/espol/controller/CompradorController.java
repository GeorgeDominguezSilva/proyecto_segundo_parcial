/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.model.Oferta;
import ec.edu.espol.model.Persona;
import ec.edu.espol.model.Vehiculo;
import ec.edu.espol.model.personas.Comprador;
import ec.edu.espol.model.vehiculos.Auto;
import ec.edu.espol.model.vehiculos.Camion;
import ec.edu.espol.model.vehiculos.Camioneta;
import ec.edu.espol.model.vehiculos.Motocicleta;
import ec.edu.espol.services.Api;
import ec.edu.espol.services.api.ApiOferta;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import javafx.scene.control.Alert;

/**
 *
 * @author Usuario
 */
public class CompradorController {
    //
    public static boolean pujar(Vehiculo vehiculo, Persona comprador, double precioPuja){
        if (precioPuja<=0){
            System.err.println("Las pujas gratis no estan permitidas en nuestro sistema");
            return false;
        }
        Oferta oferta = new Oferta();
        oferta.setComprador(comprador.getCorreo());
        oferta.setMatricula(vehiculo.getPlaca());
        oferta.setPrecio(precioPuja);
        oferta.setVendedor(vehiculo.getVendedor());
        
        if (ApiOferta.addOferta(oferta)){
            System.out.println("Has subido tu oferta de manera exitosa");
            return true;
        } else {
            System.err.println("Tu oferta no pudo ser almacenada, verifica que no sea igual a tu anterior oferta");
            return false;
        }
        
        
    }
    
    public static List<Vehiculo> getVehiculos(String tipoVehiculo, String anioInicio, String anioFin, String precioInicio, String precioFin){
        //load data        
        List<Vehiculo> vehiculos = Api.getVehiculos();
        
        //aplicar filtros
        Predicate<Vehiculo> filtro = vehiculo -> {     
            boolean response = true;
            if (!tipoVehiculo.isBlank()){
                switch (tipoVehiculo) {
                    case "Auto":
                        response = response && vehiculo instanceof Auto;
                        break;
                    case "Camion":
                        response = response && vehiculo instanceof Camion;
                        break;
                    case "Camioneta":
                        response = response && vehiculo instanceof Camioneta;
                        break;
                    case "Motocicleta":
                        response = response && vehiculo instanceof Motocicleta;
                        break;
                    default:
                        break;
                }
            }
            if (!anioInicio.isBlank() && !anioFin.isBlank()){
                try{
                    int minAnio = Integer.parseInt(anioInicio);
                    int maxAnio = Integer.parseInt(anioFin);
                    response = (response) && (vehiculo.getAnio() >= minAnio  &&  vehiculo.getAnio() <= maxAnio);
                } catch(Exception ex){}
            }
            if (!precioInicio.isBlank() && !precioFin.isBlank()){
                try{
                    double minPrecio = Double.parseDouble(precioInicio);
                    double maxPrecio = Double.parseDouble(precioFin);
                    response = (response) && (vehiculo.getPrecio() >= minPrecio  &&  vehiculo.getPrecio() <= maxPrecio);
                } catch(Exception ex){}
            }
            return response;
        } ;
        
        List<Vehiculo> response = vehiculos.stream().filter(filtro).collect(Collectors.toList());
        
        if (response!=null && response.size()>0){
            System.out.println("Se han encontrado coincidencias: ");
            return response;
        } else {
            System.err.println("No se han encontrado coincidencias: ");
            return new ArrayList<>();
        }
        
    }
    
    public static void showAlert(String title, String header, String content){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.showAndWait();
    }
    
}
