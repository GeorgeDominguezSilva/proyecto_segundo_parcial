/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.util.Objects;
/**
 *
 * @author Usuario
 */
public class Oferta {
    private String matricula;
    private String comprador;
    private String vendedor;
    private double precio;

    
    
    public Oferta() {
        this.matricula = "";
        this.precio = 0;
        this.comprador = "";
        this.vendedor = "";
    }

    public Oferta(String matricula, double precio, String comprador, String vendedor) {
        this.matricula = matricula;
        this.comprador = comprador;
        this.vendedor = vendedor;
        this.precio = precio;
    }

    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Oferta)) {
            return false;
        }
        final Oferta other = (Oferta) obj;
       
        if(!this.matricula.equals(other.matricula)){
            return false;
        }
        //aqui pasa con matricula igual
        if (comprador.equals(other.comprador)){            
            return (precio == other.precio);
        }else{
            return false;
        }
        
        /*
        System.out.println("qi 12");
        if (!Objects.equals(this.matricula, other.matricula) && !Objects.equals(this.comprador, other.comprador) && !Objects.equals(this.vendedor, other.vendedor)   ) {
            return false;
        }
        System.out.println("q3");
        return true;
        */
    }
    
    
    
    

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getComprador() {
        return comprador;
    }

    public void setComprador(String comprador) {
        this.comprador = comprador;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getVendedor() {
        return vendedor;
    }

    public void setVendedor(String vendedor) {
        this.vendedor = vendedor;
    }
    
    
    
    
    
    
    
    
}
