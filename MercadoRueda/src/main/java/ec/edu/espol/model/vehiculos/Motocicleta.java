/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model.vehiculos;

import ec.edu.espol.model.Vehiculo;

/**
 *
 * @author Usuario
 */
public class Motocicleta extends Vehiculo {

    public Motocicleta() {
        super();
    }

    public Motocicleta(String vendedor, String placa, String marca, String modelo, String tipoMotor, int anio, double recorrido, String color, String tipoCombustible, double precio, String transmision, int vidrios) {
        super(vendedor, placa, marca, modelo, tipoMotor, anio, recorrido, color, tipoCombustible, precio, transmision, vidrios);
    }
    
    //la moto no tiene vidrios
    public Motocicleta(String vendedor, String placa, String marca, String modelo, String tipoMotor, int anio, double recorrido, String color, String tipoCombustible, double precio, String transmision) {
        super(vendedor, placa, marca, modelo, tipoMotor, anio, recorrido, color, tipoCombustible, precio, transmision, 0);
    }

    
    
}
