/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model.personas;

import ec.edu.espol.model.Persona;

/**
 *
 * @author Usuario
 */
public class Vendedor extends Persona{
    
    private int calificacion;
    
    public Vendedor(Persona persona){
        super(persona);
        calificacion = 0;
    }
    
    public Vendedor(String nombre, String apellido, String correo, String organizacion, String usuario, String password) {
        super(nombre, apellido, correo, organizacion, usuario, password);
        calificacion = 0;
    }
    
    public Vendedor(String nombre, String apellido, String correo, String organizacion, String usuario, String password,int calificacion) {
        super(nombre, apellido, correo, organizacion, usuario, password);
        this.calificacion = calificacion;
    }

    public int getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }
    
    public Vendedor(){
        super();
        calificacion = 0;
    }
    
    
    
    
    
}