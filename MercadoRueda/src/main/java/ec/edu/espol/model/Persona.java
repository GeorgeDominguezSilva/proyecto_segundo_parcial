/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import ec.edu.espol.services.HashService;
import java.util.Objects;

/**
 *
 * @author Usuario
 */
public class Persona {
    private String nombre;
    private String apellido;
    private String correo;
    private String organizacion;
    private String usuario;
    private String passwordHash;
    
    public Persona(){
        this("","","","","","12345");
    }
    
    public Persona(Persona persona){
        this.nombre = persona.getNombre();
        this.apellido = persona.getApellido();
        this.correo = persona.getCorreo();
        this.organizacion = persona.getOrganizacion();
        this.usuario = persona.getUsuario();
        this.passwordHash = persona.getPasswordHash();
    }

    public Persona(String nombre, String apellido, String correo, String organizacion, String usuario, String password) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;
        this.organizacion = organizacion;
        this.usuario = usuario;
        String hash = HashService.getHash(password);
        if (hash!=null){
            passwordHash = hash;
        } else {
            String claveTemporal = "12345";
            System.err.println("No se pudo operar tu clave, se te asigno la siguiente: "+claveTemporal);
            passwordHash = HashService.getHash(claveTemporal);
        }
        
    }

    
    
    
    

    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Persona)) {
            return false;
        }
        final Persona other = (Persona) obj;
        if (!Objects.equals(this.correo, other.correo) && !Objects.equals(this.usuario, other.usuario) ) {
            return false;
        }        
        return true;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(String organizacion) {
        this.organizacion = organizacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    
    
    
    
    
    
    
}
