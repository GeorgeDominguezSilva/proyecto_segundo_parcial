/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model.personas;

import ec.edu.espol.model.Persona;

/**
 *
 * @author Usuario
 */
public class Comprador extends Persona {
    
    public Comprador(Persona persona){
        super(persona);
    }
        
    public Comprador(String nombre, String apellido, String correo, String organizacion, String usuario, String password) {
        super(nombre, apellido, correo, organizacion, usuario, password);
    }
    
    public Comprador(){
        super();
    }
    
    
    
}
