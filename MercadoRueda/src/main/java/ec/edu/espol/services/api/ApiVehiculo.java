/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.services.api;

/**
 *
 * @author Usuario
 */
import ec.edu.espol.services.*;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import ec.edu.espol.model.Vehiculo;
import ec.edu.espol.model.vehiculos.Auto;
import ec.edu.espol.model.vehiculos.Motocicleta;

/**
 *
 * @author Usuario
 */
public class ApiVehiculo {
    //
        
    //aqui va lo de personas
    public static List<Vehiculo> getVehiculos(){
        ObjectMapper mapper = new ObjectMapper();
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.EVERYTHING, JsonTypeInfo.As.PROPERTY);
        List<Vehiculo> response = new ArrayList<>();
        try{
            response = (mapper.readValue(Paths.get(Api.rutaVehiculos).toFile(), ResponseVehiculo.class)).getVehiculos();
        } catch(Exception ex){
            System.err.println("No se pudo leer el archivo: "+Api.rutaVehiculos+", " + ex.getMessage());
        }
        return response;
    }

    public static Vehiculo getVehiculo(String matricula){
        List<Vehiculo> vehiculos = getVehiculos();        
        for (Vehiculo vehiculo : vehiculos){
            if (vehiculo.getPlaca().equals(matricula)){
                return vehiculo;
            }
        }
        return null;
    }
    
    public static boolean addVehiculo(Vehiculo vehiculo){
        ObjectMapper mapper = new ObjectMapper();
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.EVERYTHING, JsonTypeInfo.As.PROPERTY);
                
        boolean response = false;
        List<Vehiculo> vehiculos = getVehiculos();
        if (vehiculo != null && !vehiculos.contains(vehiculo)){
             try{                
                if (vehiculo.getPlaca().isBlank()){
                    System.err.println("La matricula no es valido");
                    return false;
                }                
                //todo correcto continuar
                vehiculos.add(vehiculo);                
                ResponseVehiculo raw = new ResponseVehiculo(vehiculos);
                mapper.writeValue(Paths.get(Api.rutaVehiculos).toFile(), raw);
                response = true;                
            } catch(Exception ex){
                System.err.println("No se pudo escribir el archivo: "+Api.rutaVehiculos+ ", "+ex.getMessage());
            }
        }
        return response;        
    }
    
    public static boolean borrarVehiculo(String matricula){
        ObjectMapper mapper = new ObjectMapper();
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.EVERYTHING, JsonTypeInfo.As.PROPERTY);
        
        
        boolean response = false;
        List<Vehiculo> vehiculos = getVehiculos();
        
        vehiculos.removeIf(v -> matricula.equals( v.getPlaca() ));
        
        try{              
           ResponseVehiculo raw = new ResponseVehiculo(vehiculos);
           mapper.writeValue(Paths.get(Api.rutaVehiculos).toFile(), raw);
           response = true;                
       } catch(Exception ex){
           System.err.println("No se pudo escribir el archivo: "+Api.rutaVehiculos+ ", "+ex.getMessage());
       }
        
        return response;        
    }
    
    
    
    
    //main para probar
    public static void main(String[] args){
        //getVehiculos();
        
    }
    
}
