/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.services.api;

/**
 *
 * @author Usuario
 */
import ec.edu.espol.model.Vehiculo;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Usuario
 */
public class ResponseVehiculo {
    private List<Vehiculo> vehiculos;

    public ResponseVehiculo(List<Vehiculo> vehiculos) {
        this.vehiculos = vehiculos;
    }
    
    public ResponseVehiculo(){
        this.vehiculos = new ArrayList<>();
    }

    public List<Vehiculo> getVehiculos() {
        return vehiculos;
    }

    public void setVehiculos(List<Vehiculo> vehiculos) {
        this.vehiculos = vehiculos;
    }

    
    
    
    
    
    
    
}