/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.services.api;

/**
 *
 * @author Usuario
 */
import ec.edu.espol.services.*;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import ec.edu.espol.model.Persona;
import ec.edu.espol.model.personas.Comprador;
import ec.edu.espol.model.personas.Vendedor;

/**
 *
 * @author Usuario
 */
public class ApiPersona {
    //
        
    //aqui va lo de personas
    public static List<Persona> getPersonas(){
        ObjectMapper mapper = new ObjectMapper();
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.EVERYTHING, JsonTypeInfo.As.PROPERTY);
        List<Persona> response = new ArrayList<>();
        try{
            response = (mapper.readValue(Paths.get(Api.rutaPersonas).toFile(), ResponsePersona.class)).getPersonas();
        } catch(Exception ex){
            System.err.println("No se pudo leer el archivo: "+Api.rutaPersonas+", " + ex.getMessage());
        }
        return response;
    }    
    
    public static boolean addPersona(Persona persona){
        ObjectMapper mapper = new ObjectMapper();
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.EVERYTHING, JsonTypeInfo.As.PROPERTY);
                
        boolean response = false;
        List<Persona> personas = getPersonas();
        if (persona != null && !personas.contains(persona)){
             try{
                if (!EmailService.isValidEmail(persona.getCorreo())){
                    System.err.println("El email no es valido");
                    return false;
                }
                int spacesUser = persona.getUsuario().split(" ").length;
                if (spacesUser>1 || persona.getUsuario().isBlank()){
                    System.err.println("El usuario no es valido");
                    return false;
                }
                
                
                System.out.println(spacesUser);
                //todo correcto continuar
                personas.add(persona);                
                ResponsePersona raw = new ResponsePersona(personas);
                mapper.writeValue(Paths.get(Api.rutaPersonas).toFile(), raw);
                response = true;
                EmailService.send(persona.getCorreo(), "Bienvenido "+persona.getNombre()+" a nuestro sistema");
            } catch(Exception ex){
                System.err.println("No se pudo escribir el archivo: "+Api.rutaPersonas+ ", "+ex.getMessage());
            }
        }
        return response;        
    }
    
    public static Persona login(String user, String password){
        String hash = HashService.getHash(password);
        List<Persona> personas = getPersonas();
        return personas.stream()
                .filter( persona -> user.equals(persona.getUsuario()) && hash.equals(persona.getPasswordHash()) )
                .findFirst()
                .orElse(null);
    }
    
    /*
    public static boolean updatePassword(String user, String password){
        String hash = HashService.getHash(password);
        List<Persona> personas = getPersonas();
        return false;
    }
    */
    
    public static boolean update(Persona persona){
        ObjectMapper mapper = new ObjectMapper();
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.EVERYTHING, JsonTypeInfo.As.PROPERTY);
                
        boolean response = false;
        List<Persona> personas = getPersonas();
        if (persona != null && personas.contains(persona)){
             try{                
                //todo correcto continuar
                personas.remove(persona);//remueve por correo o usuario                
                personas.add(persona);//lo vuelve agregar con los nuevos campos
                ResponsePersona raw = new ResponsePersona(personas);
                mapper.writeValue(Paths.get(Api.rutaPersonas).toFile(), raw);
                response = true;
            } catch(Exception ex){
                System.err.println("No se pudo escribir el archivo: "+Api.rutaPersonas+ ", "+ex.getMessage());
            }
        }
        return response;        
    }
    
    
    
    
    
    
    
    
    //main para probar
    public static void main(String[] args){
        //Comprador comprador = new Comprador(nombre,apellido,correo,organizacion,usuario,clave);
        Comprador comprador = new Comprador("test com","test apellido","test@localhost.com","testORG","testUsuario","12345");
        //ApiPersona.addPersona(comprador);
        
        System.out.println(ApiPersona.update(new Vendedor(comprador)));        
        
        //System.out.println(getPersonas());
    }
    
}
