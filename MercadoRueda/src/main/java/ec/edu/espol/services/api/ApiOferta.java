/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.services.api;

/**
 *
 * @author Usuario
 */
import ec.edu.espol.services.*;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import ec.edu.espol.model.Oferta;
import ec.edu.espol.model.Persona;
import ec.edu.espol.model.personas.Vendedor;


/**
 *
 * @author Usuario
 */
public class ApiOferta {
    //
        
    //aqui va lo de personas
    public static List<Oferta> getOfertas(){
        ObjectMapper mapper = new ObjectMapper();
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.EVERYTHING, JsonTypeInfo.As.PROPERTY);
        List<Oferta> response = new ArrayList<>();
        try{
            response = (mapper.readValue(Paths.get(Api.rutaOfertas).toFile(), ResponseOferta.class)).getOfertas();
        } catch(Exception ex){
            System.err.println("No se pudo leer el archivo: "+Api.rutaOfertas+", " + ex.getMessage());
        }
        return response;
    }
    
    public static List<Oferta> getOfertas(String matricula, Persona vendedor){        
        List<Oferta> listaOfertas = getOfertas();
        List<Oferta> response = new ArrayList<>();        
        if (matricula.isBlank()){
            return response;
        }
        listaOfertas.stream().filter((oferta) -> (oferta.getMatricula().equals(matricula) && oferta.getVendedor().equals(vendedor.getCorreo()))).forEachOrdered((oferta) -> {
            response.add(oferta);
        });
        return response;
    }
    
    

    
    
    public static boolean addOferta(Oferta oferta){
        ObjectMapper mapper = new ObjectMapper();
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.EVERYTHING, JsonTypeInfo.As.PROPERTY);
                
        boolean response = false;
        List<Oferta> ofertas = getOfertas();
        if (oferta != null && !ofertas.contains(oferta)){
             try{
                if (!(oferta.getPrecio()>0)){
                    System.err.println("El precio debe ser mayor a cero");
                    return false;
                }
                //todo correcto continuar
                ofertas.add(oferta);                
                ResponseOferta raw = new ResponseOferta(ofertas);
                mapper.writeValue(Paths.get(Api.rutaOfertas).toFile(), raw);
                response = true;
                //aqui debemos enviar correo                
                EmailService.send(
                    oferta.getComprador(),
                    "Se ha enviado tu oferta correctamente "+oferta.getComprador()+" a nuestro sistema. Matricula: "+
                    oferta.getMatricula() + ", Precio Ofertado: " + oferta.getPrecio()
                );
                EmailService.send(
                    oferta.getVendedor(),
                    "Se ha recibido una oferta correctamente "+oferta.getVendedor()+ " de "+oferta.getComprador()+" a nuestro sistema. Matricula: "+
                    oferta.getMatricula() + ", Precio Ofertado: " + oferta.getPrecio() +
                    "\n Ingrese a la plataforma para aceptar"
                );
            } catch(Exception ex){
                System.err.println("No se pudo escribir el archivo: "+Api.rutaOfertas+ ", "+ex.getMessage());
            }
        }
        return response;        
    }
    
    
    public static boolean aceptarOferta(Oferta oferta){
        List<Oferta> ofertas = getOfertas();        
        ofertas.removeIf(of -> of.getMatricula().equals(oferta.getMatricula()));
        
        ObjectMapper mapper = new ObjectMapper();
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.EVERYTHING, JsonTypeInfo.As.PROPERTY);
                
        boolean response = false;
        
        
        try{
           //todo correcto continuar           
           ResponseOferta raw = new ResponseOferta(ofertas);
           mapper.writeValue(Paths.get(Api.rutaOfertas).toFile(), raw);
           response = true;
           //aqui debemos enviar correo   
           ApiVehiculo.borrarVehiculo(oferta.getMatricula());
           EmailService.send(
               oferta.getComprador(),
               "Se ha acepto la compra correctamente "+oferta.getComprador()+" a nuestro sistema. Matricula: "+
               oferta.getMatricula() + ", Precio Ofertado: " + oferta.getPrecio()
           );
           EmailService.send(
               oferta.getVendedor(),
               "Se acepto la compra correctamente "+oferta.getVendedor()+ " de "+oferta.getComprador()+" a nuestro sistema. Matricula: "+
               oferta.getMatricula() + ", Precio Ofertado: " + oferta.getPrecio() +
               "\n Ingrese a la plataforma para aceptar"
           );
       } catch(Exception ex){
           System.err.println("No se pudo escribir el archivo: "+Api.rutaOfertas+ ", "+ex.getMessage());
       }
        
        return response;
    }
    
    
    
    
    //main para probar
    public static void main(String[] args){
         
         
    }
    
}