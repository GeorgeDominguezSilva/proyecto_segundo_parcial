/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.services;

/**
 *
 * @author Usuario
 */
import java.time.LocalDateTime;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
//import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.validator.routines.EmailValidator;

//properties
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class EmailService {
   
   public static void main(String[] args){
       //
       /*
       File archivo = new File("archivo.properties");       
         
        System.out.println("Crearemos un nuevo archivo de configuraciones");
        try {            
            System.out.println("\tCreando el flujo de escritura a archivo");
             
            try (OutputStream outputStream = new FileOutputStream(archivo)) {
                System.out.println("\tCreando el objeto Properties");
                Properties prop = new Properties();
                 
                System.out.println("\tCreamos las claves del archivo properties y sus valores");
                // set key and value
                prop.setProperty("db.url", "google.com");
                prop.setProperty("db.usuario", "user");
                prop.setProperty("db.password", "12345");
                 
                System.out.println("\tAlmacenamos el arhivo");
                System.out.println("\tCon un comentario al inicio");
                prop.store(outputStream, "Config");
                 
                System.out.println("\tEl archivo se cierra en este punto gracias al Try-catch con recursos");
            }
             
            System.out.println("");
            System.out.println("");
            System.out.println("Leemos el archivo de configuracion");
            System.out.println("\tCreando el flujo de lectura a archivo");
             
            try (InputStream inputStream = new FileInputStream(archivo)) {
                Properties prop = new Properties();
                prop.load(inputStream);
                System.out.println("\tLeemos el valor de las claves");
                // get value by key
                System.out.println(prop.getProperty("db.url"));
                System.out.println(prop.getProperty("db.usuario"));
                System.out.println(prop.getProperty("db.password"));
            }
             
            System.out.println("");
            System.out.println("");
            System.out.println("Editaremos el valor de una clave");
            Properties prop = new Properties();
             
            try (InputStream inputStream = new FileInputStream(archivo)) {
                System.out.println("\tPrimero leeremos el archivo ya existente");
                prop.load(inputStream);
            }
       
            try (OutputStream outputStream = new FileOutputStream(archivo)) {          
                System.out.println("\tCambiamos el valor de una de las claves");
                // set key and value
                prop.setProperty("db.url", "serverPrincipal.dyndns.org");
                 
                System.out.println("\tAlmacenamos el arhivo");
                prop.store(outputStream, "Config");
            }
             
            System.out.println("");
            System.out.println("");
            System.out.println("** Mostramos las claves");           
            try (InputStream inputStream = new FileInputStream(archivo)) {
                prop = new Properties();
                prop.load(inputStream);
                System.out.println("\tLeemos el valor de las claves");
                // get value by key
                System.out.println(prop.getProperty("db.url"));
                System.out.println(prop.getProperty("db.usuario"));
                System.out.println(prop.getProperty("db.password"));
            }            
             
        } catch (FileNotFoundException ex) {
            System.out.println("Archivo no encontrado!");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Error de entrada salida!");
            ex.printStackTrace();
        }
       */
       //makeProperties();
       
   }
   
   public static Properties readProperties(){
       //
        File archivo = new File("mail.properties");       
        Properties prop = new Properties();
        
        boolean readFails = false;
        try (InputStream inputStream = new FileInputStream(archivo)) {            
            prop.load(inputStream);
        }catch (FileNotFoundException ex) {
            readFails = true;
            System.out.println("readProperties: Archivo no encontrado!");            
        } catch (IOException ex) {
            readFails = true;
            System.out.println("readProperties: Error de entrada salida!");
        }
        if (readFails){
            prop = makeProperties();
        }
        
        return prop;
   }
   
   /*
   Aqui van los metodos para crear y leer las properties del email
   */
   public static Properties makeProperties(){
       File archivo = new File("mail.properties");   
       
       Properties prop = new Properties();

        System.out.println("\tCreamos las claves del archivo properties y sus valores");
        // set key and value
        prop.setProperty("mail.from", "quevehiculomecompro");
        prop.setProperty("mail.username", "quevehiculomecompro");
        prop.setProperty("mail.password", "espol.poo.2020");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
       
       try (OutputStream outputStream = new FileOutputStream(archivo)) {
            System.out.println("\tCreando el objeto Properties");
            System.out.println("\tAlmacenamos el arhivo");
            System.out.println("\tCon un comentario al inicio");
            prop.store(outputStream, "Config");
            System.out.println("\tEl archivo se cierra en este punto gracias al Try-catch con recursos");
        } catch (FileNotFoundException ex) {
            System.out.println("Archivo no encontrado!");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Error de entrada salida!");
            ex.printStackTrace();
        }
        return prop;
       
   }
   
   
   
   
   public static void send(String to, String message){
       /*
       Thread th = new Thread(()->{
           enviar(to,message);
       });
       th.start();       
       */
       //activar para produccion
       enviar(to,message);
   }
   
   /*
         Para saber si es valido el email
   */   
   public static boolean isValidEmail(String email) {
       // create the EmailValidator instance
       EmailValidator validator = EmailValidator.getInstance();
       // check for valid email addresses using isValid method
       return validator.isValid(email);
   }
   
   
    
   public static void enviar(String to, String mensaje) {
       mensaje = "para: "+ to + "\n  "   + mensaje;
       
       Properties customProp = readProperties();
       
       System.out.println("enviando..."+mensaje);
      // Recipient's email ID needs to be mentioned.

      // Sender's email ID needs to be mentioned
      String from = customProp.getProperty("mail.from");
      
      final String username = customProp.getProperty("mail.username");//change accordingly
      final String password = customProp.getProperty("mail.password");//change accordingly

      
      //String host = "smtp.live.com";
      

      Properties props = new Properties();
      props.put("mail.smtp.auth", customProp.getProperty("mail.smtp.auth"));
      props.put("mail.smtp.starttls.enable", customProp.getProperty("mail.smtp.starttls.enable"));
      props.put("mail.smtp.host", customProp.getProperty("mail.smtp.host") );
      props.put("mail.smtp.port", customProp.getProperty("mail.smtp.port"));
      //props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

      // Get the Session object.
      Session session = Session.getInstance(props,
      //Session session = Session.getInstance(props,
         new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
               return new PasswordAuthentication(username, password);
	   }
         });

      try {
	   // Create a default MimeMessage object.
	   Message message = new MimeMessage(session);
	
	   // Set From: header field of the header.
	   message.setFrom(new InternetAddress(from));
	
	   // Set To: header field of the header.
	   message.setRecipients(Message.RecipientType.TO,
               InternetAddress.parse(to));
	
	   // Set Subject: header field
	   message.setSubject("System Delivery");
	
	   // Now set the actual message
	   //message.setText("Hola, esta es una prueba de " + "email using JavaMailAPI ");
           message.setText(mensaje);

	   // Send message
           Transport transport = session.getTransport();
           transport.connect(customProp.getProperty("mail.smtp.host"), username, password);
	   //Transport.send(message);
           transport.sendMessage(message,message.getAllRecipients());

	   System.out.println("Mensaje enviado exitosamente....");

      } catch (MessagingException e) {
         System.err.println("No se pudo enviar el correo, verifique el email o su conexion a internet");
         System.out.println(e.getMessage());
         //throw new RuntimeException(e);
      }
   }
}
