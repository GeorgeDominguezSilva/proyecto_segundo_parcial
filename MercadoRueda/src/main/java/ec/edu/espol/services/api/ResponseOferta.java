/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.services.api;

/**
 *
 * @author Usuario
 */
import ec.edu.espol.model.Oferta;
import java.util.ArrayList;
import java.util.List;



/**
 *
 * @author Usuario
 */
public class ResponseOferta {
    private List<Oferta> ofertas;

    public ResponseOferta(List<Oferta> ofertas) {
        this.ofertas = ofertas;
    }
    
    public ResponseOferta(){
        this.ofertas = new ArrayList<>();
    }

    public List<Oferta> getOfertas() {
        return ofertas;
    }

    public void setOfertas(List<Oferta> ofertas) {
        this.ofertas = ofertas;
    }

    
    
    
    
    
    
    
}