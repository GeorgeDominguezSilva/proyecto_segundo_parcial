/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.services;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import ec.edu.espol.model.Oferta;
import ec.edu.espol.model.Persona;
import ec.edu.espol.model.Vehiculo;
import ec.edu.espol.services.api.ApiOferta;
import ec.edu.espol.services.api.ApiPersona;
import ec.edu.espol.services.api.ApiVehiculo;
import java.util.List;

/**
 *
 * @author Usuario
 */
public class Api {
    //
    public static String rutaPersonas = "personas.json";
    public static String rutaVehiculos = "vehiculos.json";
    public static String rutaOfertas = "ofertas.json";
    
    public Api(){
        ObjectMapper mapper;
        mapper = new ObjectMapper();
        //mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.EVERYTHING);
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.EVERYTHING, JsonTypeInfo.As.PROPERTY);
        
        //por si deseamo usar como objecto
    }
    
    //aqui va lo de personas
    //aqui va lo de personas
    public static List<Persona> getPersonas(){        
        return ApiPersona.getPersonas();
    }
    public static boolean addPersona(Persona persona){
        return ApiPersona.addPersona(persona);
    }
    
    //aqui va lo de vehiculos
    //aqui va lo de vehiculos
    public static List<Vehiculo> getVehiculos(){        
        return ApiVehiculo.getVehiculos();        
    }
    public static boolean addVehiculo(Vehiculo vehiculo){
        return ApiVehiculo.addVehiculo(vehiculo);
    }
    
    
    //aqui va la de ofertas
    //aqui va ;a de ofertas
    public static List<Oferta> getOfertas(){        
        return ApiOferta.getOfertas();        
    }
    public static boolean addOferta(Oferta oferta){
        return ApiOferta.addOferta(oferta);
    }
    
    
    
}
