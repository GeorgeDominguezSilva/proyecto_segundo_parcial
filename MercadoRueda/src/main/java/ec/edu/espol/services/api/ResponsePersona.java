/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.services.api;

/**
 *
 * @author Usuario
 */
import ec.edu.espol.model.Persona;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Usuario
 */
public class ResponsePersona {
    private List<Persona> personas;

    public ResponsePersona(List<Persona> personas) {
        this.personas = personas;
    }
    
    public ResponsePersona(){
        this.personas = new ArrayList<>();
    }

    public List<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(List<Persona> personas) {
        this.personas = personas;
    }
    
    
    
    
    
    
}
