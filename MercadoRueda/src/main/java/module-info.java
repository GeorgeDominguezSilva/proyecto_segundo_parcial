module ec.edu.espol.gui {
    requires javafx.controls;
    requires java.mail;
    requires commons.validator;
    
    requires com.fasterxml.jackson.annotation;
    requires com.fasterxml.jackson.core;
    requires com.fasterxml.jackson.databind;
    exports ec.edu.espol.gui;
}